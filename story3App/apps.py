from django.apps import AppConfig


class Story3AppConfig(AppConfig):
    name = 'story3App'
